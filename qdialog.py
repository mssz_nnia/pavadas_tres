# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Pythonbook\PyQt5\qdialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(200, 150)
        Dialog.setMinimumSize(QtCore.QSize(200, 150))
        Dialog.setMaximumSize(QtCore.QSize(200, 150))
        self.btActualizar = QtWidgets.QPushButton(Dialog)
        self.btActualizar.setGeometry(QtCore.QRect(60, 40, 75, 23))
        self.btActualizar.setMinimumSize(QtCore.QSize(75, 23))
        self.btActualizar.setMaximumSize(QtCore.QSize(75, 23))
        self.btActualizar.setObjectName("btActualizar")
        self.btCancelar = QtWidgets.QPushButton(Dialog)
        self.btCancelar.setGeometry(QtCore.QRect(60, 80, 75, 23))
        self.btCancelar.setMinimumSize(QtCore.QSize(75, 23))
        self.btCancelar.setMaximumSize(QtCore.QSize(75, 23))
        self.btCancelar.setObjectName("btCancelar")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.btActualizar.setText(_translate("Dialog", "Actualizar"))
        self.btCancelar.setText(_translate("Dialog", "Cancelar"))