import sys

from PyQt5.QtWidgets import QMainWindow, QApplication

from PyQt5.QtSql import QSqlQuery, QSqlQueryModel

from PyQt5.QtCore import Qt

from conexion import crearConex

from dialog import * 

from tableview import Ui_MainWindow

class mainVentana(QMainWindow):

    def __init__(self):

        super(mainVentana, self).__init__()

        self.ui = Ui_MainWindow()

        self.ui.setupUi(self)

        self.modelo = QSqlQueryModel()

        query = QSqlQuery()

        query.prepare("SELECT * FROM articulo WHERE estado = 'Activo'")

        query.exec_()

        self.modelo.setQuery(query)

        cabeceras = ['Codigo',
                     'Proveedor',
                     'Rubro',
                     'Articulo',
                     'Descripcion',
                     'Costo',
                     'Ganancia',
                     'IVA',
                     'Neto',
                     'Venta',
                     'Medida',
                     'Estado']

        for index, cabeceras in enumerate(cabeceras):

            self.modelo.setHeaderData(index, Qt.Horizontal, cabeceras)


        self.vista = self.ui.tableView

        self.vista.setModel(self.modelo)

        self.vista.clicked.connect(self.iniciarDialogo)

        #Ver campo [] en fila seleccionada

        #self.vista.clicked.connect(self.verDatosFila)
        

    def iniciarDialogo(self):

        self.main_dialog = dialogVentana()


    def verDatosFila(self):

        fila = self.vista.selectedIndexes()

        print(self.modelo.data(fila[0]))
       

if __name__ == "__main__":

    app = QApplication(sys.argv)

    if not crearConex():

        sys.exit(-1)

    ventana = mainVentana()

    ventana.show()

    sys.exit(app.exec_())